%% Main testing script
% Author:
%   Matt Barnes
%   barnes.matt.j@gmail.com

clear, clc, close all

%% TO DO:
% error bars on baseline using bootstrapping
% all divergences: weight by class weight
% fix knn feature
% validation sets for SVM. Currently training and optimizing on the same
% sets
% multiclass

%% Paths
addpath('utils');
addpath('density');
addpath(genpath('~/Documents/matlab/ite/'));  % ite toolbox, for divergence estimates

%% Parameters
%approaches = {'ite-Renyi_kNN_k', 'ite-Renyi_kNN_k', 'ite-Renyi_kNN_k', 'ite-Renyi_kNN_k', 'ite-Renyi_kNN_k'};
approaches = {'ite-L2_kNN_k', 'ite-Tsallis_kNN_k', 'ite-Renyi_kNN_k', 'ite-MMD_Ustat', 'ite-MMD_Vstat', 'ite-MMD_Ustat_iChol', ...
              'ite-Hellinger_kNN_k', 'ite-Bhattacharyya_kNN_k', 'ite-KL_kNN_k', 'ite-KL_kNN_kiTi', 'ite-EnergyDist', ...
              'ite-Bregman_kNN_k', 'ite-symBregman_kNN_k', 'ite-ChiSquare_kNN_k', 'ite-SharmaM_kNN_k', 'ite-KL_expF'}; 
densitymulti = 1;
results = cell(length(approaches), 4);

%% Load data
load('../data/synthetic/hand3.mat')

%% Combine old cell format into new array format
if iscell(samples)  % 
    sets = cell(size(samples));  % which datset the sample came from
    for i = 1:length(labels)
        sets{i} = i*ones(size(labels{i}));
    end
    sets = vertcat(sets{:});
    samples = vertcat(samples{:});
    labels = vertcat(labels{:});
    spurious = vertcat(spurious{:});

    sets_testing = cell(size(samples_testing));  % which datset the sample came from
    for i = 1:length(labels_testing)
        sets_testing{i} = i*ones(size(labels_testing{i}));
    end
    sets_testing = vertcat(sets_testing{:});
    samples_testing = vertcat(samples_testing{:});
    labels_testing = vertcat(labels_testing{:});
end

parameters.ntrainsets = max(sets) - min(sets)+1;
parameters.nsamples = length(sets==sets(1));
parameters.dim = size(samples, 2);


for i = 1:length(approaches)
    approach = approaches{i}
    multiplicativeconstant = densitymulti(1);
    densityestimator = approach(5:end);  % choose from Table 4 in ITE Documentation
    densityfh = str2func(['D',densityestimator,'_initialization']);  % initialization function handle
    densitymodel = densityfh(multiplicativeconstant);
    densityfh = str2func(['D',densityestimator,'_estimation']);  % estimation function handle
    originaldivergence = itemulti(samples, labels, sets, densitymodel, densityfh);
    oracledivergence = itemulti(samples(spurious==0), labels(spurious==0), sets(spurious==0), densitymodel, densityfh);
    results{i, 1} = approach;
    results{i, 2} = multiplicativeconstant;
    results{i, 3} = originaldivergence;
    results{i, 4} = oracledivergence;
end
    
