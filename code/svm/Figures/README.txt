Fig 1:
Sample dataset 1 with spurious samples

Fig 2:
Sample dataset 2 with spurious samples

Fig 3:
Linear SVM applied to Fig 1

Fig 4:
Linear SVM applied to Fig 1 with spurious samples manually removed

Fig 5:
SVM learned in Fig 3 applied to dataset 2

Fig 6:
SVM learned in Fig 4 applied to dataset 2