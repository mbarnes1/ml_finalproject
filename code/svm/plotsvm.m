function [h] = plotsvm( model )
%PLOTSVM Plots the 2D decision boundary on the current figure
%   Input:
%       model - output from libsvm's svmtrain
%
%   Outputs:
%       h - handle of the plotted line
%
%   Author:
%       Matt Barnes

%% Calculate SVM values w and b (from the libsvm FAQ page below)
%  http://www.csie.ntu.edu.tw/~cjlin/libsvm/faq.html#f804
w = model.SVs' * model.sv_coef;
b = -model.rho;
if model.Label(1) == -1
  w = -w;
  b = -b;
end

%% Plot the decision boundary
currentaxis = axis;  % axis of the current figure, which we'll use
x           = linspace(currentaxis(1), currentaxis(2), 100);  % ...
                     % 100 samples evenly spaced along the x-axis
y = (-w(1)*x - b)/w(2);         % corresponding y values
h = plot(x,y,'b');        % plot as a blue line
axis(currentaxis)    % just to make sure the axis stay the same ;)

end

