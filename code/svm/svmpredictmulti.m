function [accuracy] = svmpredictmulti( labels, samples, model, options)
%SVMPREDICTMULTI Predicts the accuracies for a cell array of labels and
%samples given one svm model
%   Inputs:
%       labels - cell array (one per dataset) of class labels
%       samples - cell array (one per dataset) of samples (nsamples x
%       nfeatures)
%       model - model from svmtrain
%       options - same options as svmpredict
%
%   Outputs:
%       accuracy - [1 x ndatasets] array of accuracies of model evaluated on each of
%       the datasets

accuracy = NaN(1, length(labels));

for i = 1:length(labels)
    [~, acc, ~] = svmpredict(labels{i}, samples{i}, model, options);
    accuracy(i) = acc(1);
end

end

