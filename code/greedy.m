function [ test_acc, valid_perf, TPR, FPR, removed ] = greedy(samples, labels, spurious, sets, samples_testing, labels_testing, sets_testing, parameters)
%GREEDY Greedily removes samples
%   Inputs:
%       samples - (nsamples x ndim) array of samples to train and validate on
%       labels - (nsamples x 1) class labels for samples
%       spurious - (nsamples x 1) spurious labels for samples, used with ROC
%       sets - (nsamples x 1) set labels for samples
%       samples_testing - (ntestsamples x ndim) array of samples to test on
%       labels_testing - (ntestsamples x 1) array of labels to test on
%       sets_testing - (ntestsamples x 1) array of set labels
%       parameters - structure with training and testing options
%           --> .approaches
%           --> .k
%           --> .plots
%           --> .nremove
%           --> .trainset
%           --> .svmoptions
%           --> .ndatasets
%           --> .hdebins
%           --> .file (unused in this function)
%   Outputs:
%       test_acc - (#remove+1 x 1) array of test accuracies
%       valid_perf - (#remove+1 x 1) array of validation
%                    performances (e.g. divergence). First entry is initial
%                    performances
%       TPR - (#remove x 1) array of TPR
%       FPR - (#remove x 1) array of FPR
%       removed - (#remove x 1) array of removed samples indices from trainset
%
%   Author:
%       Matt Barnes
%
%   Question: Test on testsets individually or all testsets combined?
%   Current: individually

%% Parameters
approach   = parameters.approaches{1};
k          = parameters.k;
plots      = false;  % whether to make plot
nremove    = parameters.nremove;
svmoptions = parameters.svmoptions;
ntestsets  = max(sets_testing);
axisAll    = parameters.axisAll;

%% Initializations
removed = NaN(nremove, 1);  % initialize indices of removed samples
remaining = 1:length(labels);

valid_perf = zeros(int16(nremove/k)+1, 1);  % initialize validation performance
% after each iteration. Accuracy for SVM, 1/divergence for density
% Greedily maximizing this value
samples_remaining = samples(remaining, :);
labels_remaining  = labels(remaining,:);
sets_remaining = sets(remaining,:);

test_acc = NaN(int16(nremove/k)+1, 1);  % performance on the testing sets
if strcmp(approach,'density_hde')
    parameters.hdebins = {linspace(axisAll(1),axisAll(2),10), linspace(axisAll(3),axisAll(4),10)};
    hdebins = parameters.hdebins;
end


%% Initial performance, prior to removing any points
model = libsvmtrain(labels, samples, svmoptions);
[~, acc, ~] = svmpredict(labels_testing, samples_testing, model, '-q');
test_acc(1) = acc(1);
if plots
    figure(fh_scat);
    subplot(1,ntestsets,trainset);
    h = plotsvm(model);
end
if strcmp('svm', approach)
    valid_perf(1) = svmpredict(labels, samples, model,'-q');
elseif strcmp('density_hde', approach)
    valid_perf(1) = 1/klHDEmulti(samples, labels, sets, hdebins);
elseif ~isempty(strfind(approach, 'ite-'))
    densitymulti = 1;  % multiplicative constant:  default = 1
    densityestimator = approach(5:end);  % choose from Table 4 in ITE Documentation
    densityfh = str2func(['D',densityestimator,'_initialization']);  % initialization function handle
    densitymodel = densityfh(densitymulti);
    densityfh = str2func(['D',densityestimator,'_estimation']);  % estimation function handle
    valid_perf(1) = 1./(itemulti(samples, labels, sets, densitymodel, densityfh));
else
    error('Invalid approach string');
end


%% Loop through number of points to remove
for iRemove = 1:int16(nremove/k)
    fprintf('   --> Removing point %i of %i\n',iRemove*k,nremove);

    temp_perf = NaN(length(samples_remaining), 1);  % accuracies after removing each sample point
    
    % Loop through each remaining point
    for iRemaining = 1:length(samples_remaining)
        %fprintf('Testing %i of %i\n',iRemaining, length(samples_remaining));
        
        temp_samples = samples_remaining;
        to_remove = knnsearch(samples_remaining,samples_remaining(iRemaining,:),'K',k);  % remove j'th sample and its k-nearest neighbors
        
        temp_samples(to_remove,:) = [];  % remove this sample
        temp_labels               = labels_remaining;
        temp_labels(to_remove,:)  = [];  % remove this label
        temp_sets                 = sets_remaining;
        temp_sets(to_remove,:)    = [];  % remove this set label
        
        if strcmp('svm', approach)
            model = libsvmtrain(temp_labels, temp_samples, svmoptions);  % train a linear classifier
            temp_perf(iRemaining) = svmpredictmulti(temp_labels, temp_samples, model,'-q');
        %elseif strcmp('density_hde', approach)
        %    temp_perf(iRemaining) = 1/klHDEmulti(temp_samples, temp_labels, temp_sets, hdebins);
        elseif ~isempty(strfind(approach, 'ite-'))
            temp_perf(iRemaining) = 1./(itemulti(temp_samples, temp_labels, temp_sets, densitymodel, densityfh));
        end
    end
    [~, iAcc] = max(temp_perf);
    valid_perf(iRemove+1, :) = temp_perf(iAcc, :);
    iAcc = knnsearch(samples_remaining, samples_remaining(iAcc,:),'K',k);

    % Remove point that increases accuracy the most
    removed(iRemove) = remaining(iAcc);
    remaining(iAcc) = [];
    
    samples_remaining = samples(remaining, :);
    labels_remaining  = labels(remaining,:);
    sets_remaining = sets(remaining,:);
    
    % Testing sets
    model = libsvmtrain(labels_remaining, samples_remaining, svmoptions);
    [~, acc, ~] = svmpredict(labels_testing, samples_testing, model, '-q');
    test_acc(iRemove+1, :) = acc(1);

    % Replot scatter and SVM
    if plots
        % -- Scatter Plot
        figure(fh_scat)
        subplot(ceil(ntestsets/ceil(sqrt(ntestsets))), ceil(sqrt(ntestsets)), trainset);
        delete(h)  % remove the previous SVM decision boundary
        scatter(samples{trainset}(removed(end-length(iAcc)+1:end),1), ...
            samples{trainset}(removed(end-length(iAcc)+1:end),2),75,'bo');...
            % plot blue circles around the removed points
        %legend('Non-spurious','Spurious','Removed Sample');
        % SVM model of the remaining points
        h = plotsvm(model);
    end
    
end


[TPR, FPR] = roccurve(removed, spurious);
        
end
