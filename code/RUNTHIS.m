%% Main testing script
% Author:
%   Matt Barnes
%   barnes.matt.j@gmail.com

clear, clc, close all

%% TO DO:
% error bars on baseline using bootstrapping
% all divergences: weight by class weight
% fix knn feature
% validation sets for SVM. Currently training and optimizing on the same
% sets
% multiclass

%% Paths
addpath('utils');
addpath('svm');  % always needed for evaluating testing accuracy
addpath('density_hde');
addpath('density');
addpath('benchmarks');
%addpath(genpath('~/Documents/matlab/ite/'));  % ite toolbox, for divergence estimates
%addpath(genpath('~/Documents/matlab/git/'));  % git toolbox, for getting latest commit hash
%addpath(genpath('~/Documents/matlab/libsvm-3.18/matlab'));  % svm toolbox
%addpath(genpath('~/Documents/matlab/odToolbox/'));  % competitor
%addpath(genpath('~/Documents/matlab/kdtree1.2/'));  % used by competitor
%addpath(genpath('~/Documents/matlab/outlier-detection-toolbox/'));  % outlier detection toolbox

addpath(genpath('/home/scratch/mbarnes1/matlab/ite/'));  % ite toolbox, for divergence estimates
addpath(genpath('/home/scratch/mbarnes1/matlab/git/'));  % git toolbox, for getting latest commit hash
addpath(genpath('/home/scratch/mbarnes1/matlab/libsvm-3.18/matlab/'));  % svm toolbox
addpath(genpath('/home/scratch/mbarnes1/matlab/odToolbox/'));  % competitor
addpath(genpath('/home/scratch/mbarnes1/matlab/kdtree1.2/'));  % used by competitor
addpath(genpath('/home/scratch/mbarnes1/matlab/outlier-detection-toolbox/'));  % outlier detection toolbox

%% Parameters
parameters.comments = '';
parameters.nboot = 10;  % number of bootstraps to execute
parameters.datafile = '../data/school/schooldata.mat'; %'../data/nuclear/ERNIE_data.mat';    % data to load samples from, new synthetic data if null
parameters.approaches = {'ite-EnergyDist'}; %symBregman_kNN_k'}; %{'ite-Renyi_kNN_k'};  % new, greedy approaches to compare
                    % Options:
                    %   density_hde - custom kl using HDEs. Only for 2 dims
                    %   (for now)
                    %   svm - untested for most recent code
                    %   Any ITE divergence estimate, see Table 4 of ITE
                    %   documentation (ite-<cost_name>)
                    %   ITE KL not working... returning infinite?
parameters.ntrainsets  = 10;     % default number of training + validation datasets (e.g. patients, folds)
parameters.ntestsets = parameters.ntrainsets;  % for now, add this feature later
parameters.k      = 1;  % BROKEN. DO NOT CHOOSE VALUE HIGHER THAN 1
                   % number of samples to analyze and remove per iteration
                   % i.e. k Nearest Neighbors - 1
                   % i.e. 1 corresponds to just the sample, 2 corresponds
                   % to the sample and its 1 nearest neighbor
parameters.nremove    = 250;    % number of samples to remove
parameters.nsamples   = 60; % default number of samples per dataset
parameters.ndim = 3;  % default number of dimensions
parameters.svmoptions = '-q -t 0';  % libsvm options:
                                        % -t 0: linear decision boundary
                                        % c: is cost for points outside
                                        % margin
                                 % Used for classification evaluation AND
                                 % in our SVM approach
parameters.alpha = .05;  % for 95% confidence interval for plots
parameters.resultsfile = ['../data/school/results',datestr(now,'yyyy_mm_dd-HHMMSS')];
parameters.git = git('rev-parse HEAD');  % latest git commit hash
parameters.cimethod = 'bootcper';  % confidence interval method
parameters.competitorinterval = 1;  % evaluate the competitors everytime 5 samples are removed
plots      = false;  % whether to make plot
numCores   = 10;


%% Data generation or loading
if strfind(parameters.datafile, 'ERNIE') % nuclear data
    [samples, labels, sets, samples_testing, labels_testing, sets_testing] = getnuclear(parameters.datafile,parameters.nsamples);
    spurious = zeros(size(labels));
elseif strfind(parameters.datafile, 'brain') %brain data
    [samples, labels, samples_testing, labels_testing] = getbrain(parameters.datafile, parameters.nsamples);
    spurious = cell(size(samples));
    for i = 1:length(samples)
        spurious{i} = zeros(size(labels{i}));
    end
elseif strfind(parameters.datafile, 'school') %school data
    [samples, labels, samples_testing, labels_testing] = getschool(parameters.datafile, parameters.nsamples);
    spurious = cell(size(samples));
    for i = 1:length(samples)
        spurious{i} = zeros(size(labels{i}));
    end
elseif strfind(parameters.datafile, 'pig') %school data
    [samples, labels, samples_testing, labels_testing] = getpiggy(parameters.datafile);
    spurious = cell(size(samples));
    for i = 1:length(samples)
        spurious{i} = zeros(size(labels{i}));
    end
elseif ~isempty(parameters.datafile)  % previous synthetic datafile
    load(parameters.datafile);
else  % no data file specified, generate new synthetic data
    nspurious  = 1;     % (synthetic only)number of spurious components in each model
    [samples_testing, labels_testing, ~] = synthetic(parameters.nsamples,parameters.ntrainsets);
    [samples, labels, spurious] = synthetic(parameters.nsamples,parameters.ntrainsets);
end


%% Multithreading

if matlabpool('size') ~= numCores && matlabpool('size') ~= 0 % checking to see if my pool is already open
    matlabpool close
    matlabpool('local',numCores);
elseif matlabpool('size') == 0
    matlabpool('local',numCores);
end


%% Combine old cell format into new array format
if iscell(samples)  % 
    sets = cell(size(samples));  % which datset the sample came from
    for i = 1:length(labels)
        sets{i} = i*ones(size(labels{i}));
    end
    sets = vertcat(sets{:});
    samples = vertcat(samples{:});
    labels = vertcat(labels{:});
    spurious = vertcat(spurious{:});

    sets_testing = cell(size(samples_testing));  % which datset the sample came from
    for i = 1:length(labels_testing)
        sets_testing{i} = i*ones(size(labels_testing{i}));
    end
    sets_testing = vertcat(sets_testing{:});
    samples_testing = vertcat(samples_testing{:});
    labels_testing = vertcat(labels_testing{:});
end

parameters.ntrainsets = max(sets) - min(sets)+1;
parameters.nsamples = length(sets==sets(1));
parameters.dim = size(samples, 2);

axisMin = min(samples, [], 1);
axisMax = max(samples, [], 1);

parameters.axisAll = [axisMin(1), axisMax(1), axisMin(2), axisMax(2)];
clear axisMin axisMax

%% Bootstrapping

[boot_test_acc, boot_valid_perf, boot_TPR, boot_FPR, boot_removed, competitors] = ...
   bootstrpcustom(parameters.nboot, @greedy, samples, labels, spurious, sets, ...
   samples_testing, labels_testing, sets_testing, parameters);
fprintf('Nonbootstrap - for bias correction\n');
[test_acc,valid_perf,TPR,FPR,removed] = greedy(samples, labels, spurious, sets, samples_testing, labels_testing, sets_testing, parameters);
for i = 1:length(competitors)
    [competitors{i}.acc, competitors{i}.tpr, competitors{i}.fpr] = ...
        evaluatebenchmark(competitors{i}.name, samples, labels, spurious, ...
        samples_testing, labels_testing, parameters);
end


%% Confidence Intervals
test_acc_ci = bootciswitch(test_acc, boot_test_acc, parameters.alpha, parameters.cimethod);
for i = 1:length(competitors)
    competitors{i}.acc_ci = bootciswitch(competitors{i}.acc, competitors{i}.bootacc, parameters.alpha, parameters.cimethod);
    competitors{i}.tpr_ci = wilsonci(competitors{i}.tpr, sum(spurious));
    competitors{i}.fpr_ci = wilsonci(competitors{i}.fpr, length(spurious)-sum(spurious));
end
    
valid_perf_ci = bootciswitch(valid_perf, boot_valid_perf, parameters.alpha, parameters.cimethod);

% ROC Wilson Confidence Interval -- does not require bootstrapping
ci_tpr = wilsonci(TPR, sum(spurious));
ci_fpr = wilsonci(FPR, length(spurious)-sum(spurious));


%% Housekeeping

if isempty(parameters.datafile)  % is this new synthetic data?
    parameters.datafile = sprintf('../data/synthetic/data%i.mat',sum(floor(1000*clock)));
    save(parameters.datafile, 'samples_testing', 'labels_testing', 'sets_testing', 'samples', 'labels', 'sets', 'spurious', 'axisAll');
end

save(parameters.resultsfile, 'parameters', ...
    'test_acc','valid_perf','TPR','FPR','removed',...
    'boot_test_acc', 'boot_valid_perf', 'boot_TPR', 'boot_FPR', ...
    'boot_removed', 'competitors', 'test_acc_ci', 'valid_perf_ci', ...
    'ci_tpr', 'ci_fpr');
 

if matlabpool('size') ~= 0
    matlabpool close
end
