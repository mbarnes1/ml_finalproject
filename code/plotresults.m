% How to use this script:
% Load the results file manually
% Run this script! Easy.

% -- Test accuracy plot --
figure;
hold all
x = (0:int16(parameters.nremove/parameters.k)).*parameters.k;
u = mean(boot_test_acc, 2);  % mean
errorbar(x, u, u-test_acc_ci(:,1), test_acc_ci(:,2)-u);
legends = cell(1+length(competitors),1);
legends{1} = 'Greedy';
for i = 1:length(competitors)  % competitors
    u = mean(competitors{i}.acc, 2);
    x = competitors{i}.x;
    errorbar(x, u, u-competitors{i}.acc_ci(:,1), competitors{i}.acc_ci(:,2)-u);
    legends{i+1} = competitors{i}.name;
end
legend(legends);
xlabel('Point Removed');
ylabel('Accuracy');
title('Testset Accuracy');


%-- Validation performance plot (what we're greedily optimizing)
figure;
x = (0:int16(parameters.nremove/parameters.k)).*parameters.k;
u = mean(boot_valid_perf, 2);  % mean
errorbar(x, u, u-valid_perf_ci(:,1), valid_perf_ci(:,2)-u);
xlabel('Point Removed');
ylabel('Validation Performance');
title('Validation Set Performance');


% -- ROC --
fh_roc = figure; hold on
subplot(1, 2, 1); hold on
plot(FPR, TPR,'b-')
plot(ci_fpr(:,1), ci_tpr(:,2),'r.');  % upper left ci
plot(ci_fpr(:,2), ci_tpr(:,1),'r.');  % lower right ci
xlabel('False Positive Rate');
ylabel('True Positive Rate');
axis([0 1 0 1])


subplot(1, 2, 2); hold on
semilogx(FPR, TPR,'b-')
semilogx(ci_fpr(:,1), ci_tpr(:,2),'r.');  % upper left ci
semilogx(ci_fpr(:,2), ci_tpr(:,1),'r.');  % lower right ci
xlabel('Log False Positive Rate');
suptitle('Spurious Detection ROC Curve');