function [ ind ] = Common( ~, ~, spurious, ~ )
%COMMON Removes all spurious samples
%   Inputs:
%       samples - (#samples x #dim) array of samples
%       labels - (#samples x 1) vector of class labels
%       spurious - (#samples x 1) vector of spurious labels
%       nremove - (scalar) number of samples to remove
%
%   Outputs:
%       ind - indices of samples (logical, 0 = removed, 1 = not removed)
%
%   Author:
%       Matt Barnes

ind = logical(abs(spurious-1));


end

