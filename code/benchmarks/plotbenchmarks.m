function [] = plotbenchmarks( benches, x )
%PLOTBENCHMARKS Plots benchmarks benches (cell array of structures) over
%interval x on the current figure
%   Inputs:
%       benches - (nbenches x 1) cell array of structures (fields: name,
%       accuracy (ndatasets x 1), samples (nsamples*ndatasets x 1))
%       x - locations to plot at
%       
%   Outputs:
%
%   Author:
%       Matt Barnes

names = cell(length(benches), 1);

for i = 1:length(benches)
    plot(x, ones(size(x))*mean(benches{i}.accuracy), 'LineWidth', 2);
    names{i} = benches{i}.name;
end
legend(names);

end

