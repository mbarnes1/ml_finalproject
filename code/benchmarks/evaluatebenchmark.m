function [ accuracy, tpr, fpr ] = evaluatebenchmark( name, samples, labels, spurious,...
    samples_testing, labels_testing, parameters )
%EVALUATEBENCHMARK Implements a variety of competing algorithms
%   Inputs:
%       name - string of the benchmark to evaluate, must be same as name of
%       function to evaluate!
%       samples - array of samples (nsamples x nfeatures)
%       labels - array of class labels (nsamples x 1)
%       spurious - array of spurious labels (nsamples x1)
%       samples_testing - array of testing samples (nsamples x nfeatures)
%       labels_testing - array of testing labels (nsamples x 1)
%       svmoptions - string of SVM options, same format as svmpredict
%
%   Outputs:
%       acc - (floor((nremove-1)/parameters.competitorspacing) x 1) vector
%       of the accuracy
%       tpr - (floor((nremove-1)/parameters.competitorspacing) x 1) vector
%       of true positive rates
%       fpr - (floor((nremove-1)/parameters.competitorspacing) x 1) vector
%       of false positive rates
%
%   Author:
%       Matt Barnes

nremove = parameters.nremove;
svmoptions = parameters.svmoptions;
spacing = parameters.competitorinterval;

x = 1:spacing:nremove;  % number of samples to remove

% Initialize vectors
accuracy = NaN(length(x),1);
tpr = NaN(length(x),1);
fpr = NaN(length(x),1);

fh = str2func(name);

%% Performance
for iX = 1:length(x);  % loop through each number of samples to remove
    s = fh(samples, labels, spurious, nremove);
    model = libsvmtrain(labels(s), ...
        samples(s), svmoptions);  % train a linear classifier
    [~, acc, ~] = svmpredict(labels_testing, samples_testing, model, '-q');
    accuracy(iX) = acc(1);
    tpr(iX) = sum(spurious(s))/sum(spurious);
    fpr(iX) = sum(spurious == 0 & s == 1)/sum(~spurious);
end

end

