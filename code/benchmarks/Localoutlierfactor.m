function [rem] = Localoutlierfactor(trs,lab,spur,num2rem)

x=trs(1:int16(ceil(length(trs)/2)),:);
y=trs(int16(ceil(length(trs)/2)+1):end,:);
%x=[]
%for i=1:int16(ceil(length(trs)/2))
%	x=[x;trs{i}];
%end

%y=[]
%for i=int16(ceil(length(trs)/2)+1):length(trs)
%	y=[y;trs{i}];
%end

dataset.trainx=x;
dataset.testx=y;

dataset2.trainx=y;
dataset2.testx=x;

%dataset
%dataset2

%pause

params.minptslb=5;
params.minptsub=15;
params.ptsStep=1;
params.theta=1.25;

r=LocalOutlierFactor(dataset,params);
r2=LocalOutlierFactor(dataset2,params);

z=[r.yprob;r2.yprob];

[zs,sidx]=sort(z,'descend');

to_rem=sidx(1:num2rem);

rem=ones(size(r.yprob,1)+size(r2.yprob,1),1);
%sum(rem)
rem(to_rem)=0;
rem=rem==1;
%sum(rem)
%pause
%size(rem)
%size(rem)


