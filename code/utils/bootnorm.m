function [ci,bstat] = bootnorm(obsstat,bstat,alpha)
% normal approximation interval
% A.C. Davison and D.V. Hinkley (1996), p198-200
 

se = std(bstat,0,1);   % standard deviation estimate
bias = mean(bsxfun(@minus,bstat,obsstat),1);
za = norminv(alpha/2);   % normal confidence point
lower = obsstat - bias + se*za; % lower bound
upper = obsstat - bias - se*za;  % upper bound

% return
ci = [lower;upper];        
end   % bootnorm() 