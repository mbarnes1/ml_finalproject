% Parameters
nremove = 600;  % number of samples to remove
inname = 'blah blah' % insert main csv filename here

% Processing
load('results1.mat','iRemoved');
load('feats_6a39_f110.mat');  % arbitrary, just used to get the fold indexes
original_ind = find(feats_6a39_f110(:,1)==4);  % original indices of fold 4
desired_ind = original_ind;
desired_ind(iRemoved(1:nremove)) = [];  % remove spurious samples

outname = 'output.txt';  % outputs desired lines

inID = fopen(inname);
outID = fopen(outname,'w');

line = fgets(inID);  % this is the header line (line 0)
line = fgets(inID);  % this is the first actual line (line 1)
lineNumber = 1;

while ischar(line)  % loop through until end of file
    if(any(line==desired_ind))  % is this a line we kept?
        fprintf(outID,line);  % print it to the output file
    end
    line = fgets(inID);
    lineNumber = line+1;
end