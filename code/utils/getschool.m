%i**********************************************************************************
%
%	LOAD_DATA
%
%		Function used to load an incoming data set and prepare it for
%		spurious sample removal
%
%		INPUT: filepath -> path to the data
%
%		OUTPUT: samples -> cell array


function [loo_train_sets,loo_train_lab,loo_test_sets,loo_test_lab] = getschool(filepath,numSamples)

load(filepath);
%load(filepath_test);

%make sure filepath_train loads an array named tr_data
%make sure filepath_test loads an array named te_data

%the first column of each set of data will be the setID or foldID..

idx=unique(data(:,1));

%te_idx=unique(te_data(:,1));
assign_idx=randperm(length(idx));
assign_idx=idx(assign_idx);

numSets=size(idx,1);
%numSets_te=size(te_idx,1);



%ind_sets=cell(numSets,1);
%ind_sets_l=cell(numSets,1);

loo_train_lab=cell(1);
loo_train_sets=cell(1);
%loo_test_sets=cell(1);
%loo_test_lab=cell(1);

for i=1:int16(ceil(numSets/2))
    %loo_train_sets{i,1}=ind_sets{assign_idx(i)};
    
	temp=data(data(:,1)==assign_idx(i),2:end-1);
	templ=data(data(:,1)==assign_idx(i),end);
	y=randsample(size(temp,1),numSamples);
	loo_train_sets{i,1}=temp(y,:);
	loo_train_lab{i,1}=templ(y);
end
%At this point we should have all individual sets separated


for i=int16(ceil(numSets/2))+1:numSets
    temp=data(data(:,1)==assign_idx(i),2:end-1);
    templ=data(data(:,1)==assign_idx(i),end);
    y=randsample(size(temp,1),numSamples);
    loo_test_sets{i-int16(ceil(numSets/2)),1}=temp(y,:);
    loo_test_lab{i-int16(ceil(numSets/2)),1}=templ(y);
end

%for i=1:numSets
%	m1=0;
%	for j=1:numSets
%		if(i~=j)
%			loo_train_sets{j-m1,i}=ind_sets{j};
%			loo_train_lab{j-m1,i}=ind_sets_l{j};
%		else
%			m1=1;
%		end
%	end
end
