function [ TPR, FPR ] = roccurve( removed, label )
%ROCCURVE Returns TPR and FPR for a spurious detection ROC curve
%   Inputs:
%       removed - (nremoved x 1) vector of the removed sample indices.
%       Remember, not necessarily all samples are removed (i.e. ROC curve
%       not to completion)
%       label - spurious label (0 = nonspurious, 1 = spurious)
%
%   Outputs:
%       TPR - true positive rate (length(removed)+1 x1)
%       FPR - false positive rate (lenght(removed)+1 x1)
%           *note: one extra entry corresponds to origin

TPR = zeros(length(removed)+1, 1);
FPR = zeros(length(removed)+1, 1);

for i = 1:length(removed)
    if label(removed(i)) == 1  % correctly removed spurious point
        TPR(i+1) = 1;
    else
        FPR(i+1) = 1;
    end
end
TPR = cumsum(TPR)/sum(label);
FPR = cumsum(FPR)/(length(label)-sum(label));

end

