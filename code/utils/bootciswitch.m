function confidence = bootciswitch(acc, bootacc, alpha, method)
%BOOTCISWITCH Computes bootstrap confidence intervals
%
%   Inputs:
%       acc - (nremove+1 x 1) vector of accuracies (or performances)
%       bootacc - (nsets+1 x nboot) array of bootstrap accuracies (or performances)
%       alpha - alpha value for confidence interval (.05 = 95%)
%       method - string for method. options: bootper, bootnorm, bootcper
%                more available in source code of MATLAB function bootci
%
%   Outputs:
%       confidence - (2 x nremove+1) array of confidence intervals,
%       first row is lower second row is upper
%
%   Author:
%       Matt Barnes

switch method
    case 'bootnorm'
        confidence = bootnorm(acc', bootacc', alpha)';
    case 'bootper'
        confidence = bootper(bootacc', alpha)';
    case 'bootcper'
        confidence = bootcper(acc', bootacc', alpha)';
end
