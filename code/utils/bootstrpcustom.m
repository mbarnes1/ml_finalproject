function [ test_acc, valid_perf, TPR, FPR, removed, competitors ] = bootstrpcustom(nboot, fh, samples, labels, spurious, sets, ...
    samples_testing, labels_testing, sets_testing, parameters)
%BOOTSTRPCUSTOM Bootstrapping of the greedy sample removal algorithm
%   Inputs:
%       nboot - number of bootstraps to execute
%       fh - function handle to bootstrap, in this case 'greedy'
%       samples - (ndatasets x 1) cell array of (nsamples x ndim) array of
%       samples to train and validate on
%       labels - (ndatasets x 1) cell array of (nsamples x 1) class labels for samples
%       samples_testing - (ndatasets x 1) cell array of (nsamples x ndim) array of
%       samples to test on
%       labels_testing - (ndatasets x 1) cell array of (nsamples x 1) array of
%       labels to test on
%
%   Outputs:
%       test_acc - (nremove+1 x nboot) test accuracies
%       valid_perf - (rremove+1 x nboot) validation performances
%       TPR - (nremove+1, nboot) array of TPR of sample removal from
%       testsample
%       FPR - (nremove+1, nboot) array of FPR of sample removal from
%       removed - (#remove x #nboot) cell array of (trainset, sampleindex)
%       samples removed
%
%       Question: When bootstrapping, should I be sampling from individual
%       datasets or the entire dataset? Training, testing, or both?

nremove = parameters.nremove;
test_acc = NaN(nremove+1, nboot);
valid_perf = NaN(nremove+1, nboot);
TPR = NaN(nremove+1, nboot);
FPR = NaN(nremove+1, nboot);
removed = NaN(nremove, nboot);
replacement = true;

% Initialize competititors
competitors{1}.name = 'Baseline';
competitors{2}.name = 'Common';
competitors{3}.name = 'Localreconstruction';
competitors{4}.name = 'Localoutlierfactor';

for i = 1:length(competitors)
    competitors{i}.bootacc = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
    competitors{i}.boottpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
    competitors{i}.bootfpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
    competitors{i}.x = 1:parameters.competitorinterval:nremove;
end

baselinebootacc = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
baselineboottpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
baselinebootfpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);

commonbootacc = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
commonboottpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
commonbootfpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);

lrbootacc = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
lrboottpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
lrbootfpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);

lofootacc = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
lofoottpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);
lofootfpr = NaN(floor((nremove-1)/parameters.competitorinterval)+1, nboot);

%% Bootstrapping
parfor i = 1:nboot
    fprintf('Bootstrap %i of %i\n',i,nboot);
    n = size(samples, 1);
    k = n;
    y = randsample(n, k, replacement);
    bootsamples = samples(y, :);
    bootlabels = labels(y);
    bootspurious = spurious(y);
    bootsets = sets(y);
    
    %% Benchmarks
    [baselinebootacc(:,i), baselineboottpr(:,i), ...
            baselinebootfpr(:,i)] = ...
            evaluatebenchmark('Baseline', bootsamples, bootlabels, bootspurious,...
            samples_testing, labels_testing, parameters);
    [commonbootacc(:,i), commonboottpr(:,i), ...
            commonbootfpr(:,i)] = ...
            evaluatebenchmark('Common', bootsamples, bootlabels, bootspurious,...
            samples_testing, labels_testing, parameters);
    [lrbootacc(:,i), lrboottpr(:,i), ...
             lrbootfpr(:,i)] = ...
             evaluatebenchmark('Localreconstruction', bootsamples, bootlabels, bootspurious,...
             samples_testing, labels_testing, parameters);
     
    [lofootacc(:,i), lofoottpr(:,i), ...
             lofootfpr(:,i)] = ...
             evaluatebenchmark('Localoutlierfactor', bootsamples, bootlabels, bootspurious,...
             samples_testing, labels_testing, parameters);
         
    %% Our approach
    [test_acc(:, i), valid_perf(:, i), TPR(:, i), FPR(:, i), removed(:, i)] = ...
        fh(bootsamples, bootlabels, bootspurious, bootsets, ...
        samples_testing, labels_testing, sets_testing, parameters);  
         
end

competitors = cell(3, 1);
competitors{1}.name = 'Baseline';
competitors{1}.bootacc = baselinebootacc;
competitors{1}.boottpr = baselineboottpr;
competitors{1}.bootfpr = baselinebootfpr;
competitors{1}.x = 1:parameters.competitorinterval:nremove;

competitors{2}.name = 'Common';
competitors{2}.bootacc = commonbootacc;
competitors{2}.boottpr = commonboottpr;
competitors{2}.bootfpr = commonbootfpr;
competitors{2}.x = 1:parameters.competitorinterval:nremove;

competitors{3}.name = 'Localreconstruction';
competitors{3}.bootacc = lrbootacc;
competitors{3}.boottpr = lrboottpr;
competitors{3}.bootfpr = lrbootfpr;
competitors{3}.x = 1:parameters.competitorinterval:nremove;

competitors{4}.name = 'Localoutlierfactor';
competitors{4}.bootacc = lrbootacc;
competitors{4}.boottpr = lrboottpr;
competitors{4}.bootfpr = lrbootfpr;
competitors{4}.x = 1:parameters.competitorinterval:nremove;

end
