%i**********************************************************************************
%
%	LOAD_DATA
%
%		Function used to load an incoming data set and prepare it for
%		spurious sample removal
%
%		INPUT: filepath -> path to the data
%
%		OUTPUT: samples -> cell array


function [samples, labels, samples_testing, labels_testing, train_foldID, test_foldID] = getpiggy(filepath)

load(filepath);
%load(filepath_test);

%make sure filepath_train loads an array named tr_data
%make sure filepath_test loads an array named te_data

%the first column of each set of data will be the setID or foldID..

idx=unique(data(:,1))
%te_idx=unique(te_data(:,1));
assign_idx=randperm(length(idx));
assign_idx=idx(assign_idx)

numSets=size(idx,1)
%numSets_te=size(te_idx,1);


split_train=cell(1);
split_test=cell(1);
split_train_l=cell(1);
split_test_l=cell(1);
train_foldID=[];
test_foldID=[];


for i=1:ceil(numSets/2)
	single_set=data(data(:,1)==assign_idx(i),:);
	single_labels=data(data(:,1)==assign_idx(i),end);
	split_train{i,1}=single_set;
	split_train_l{i,1}=single_labels;
	train_foldID=[train_foldID; assign_idx(i)];
end

for i=ceil(numSets/2)+1:numSets
	single_set=data(data(:,1)==assign_idx(i),:);
	single_labels=data(data(:,1)==assign_idx(i),end);
	split_test{i-ceil(numSets/2),1}=single_set;
	split_test_l{i-ceil(numSets/2),1}=single_labels;
	test_foldID=[test_foldID; assign_idx(i)];
end

samples=split_train;
labels=split_train_l;
samples_testing=split_test;
labels_testing=split_test_l;





