function [samples, labels, spurious] = syntheticv2(nsamples, ndatasets)
%SYNTHETICV2 Generates synthetic samples in a new fashion
%   Inputs:
%       nsamples - number of nonspurious points per dataset
%       ndatasets - number of datasets to create
%
%   Outputs:
%       samples - cell array of samples
%       labels - cell array of spurious labels
%       spurious - cell array of spurious labels
%
%   Author:
%       Matt Barnes
figure, hold on
samples = cell(ndatasets, 1);
labels = cell(ndatasets, 1);
spurious = cell(ndatasets, 1);

for i = 1:ndatasets
    nonspurious1 = bsxfun(@times, (bsxfun(@plus, rand(int32(nsamples/2), 2), [0 -0.5])),[-1, 2]);
    nonspurious2 = bsxfun(@times, (bsxfun(@plus, rand(int32(nsamples/2), 2), [0 -0.5])),[1, 2]);
    plot(nonspurious1(:,1), nonspurious1(:, 2),'r.');
    plot(nonspurious2(:,1), nonspurious2(:, 2),'b.');
    axis([-1 1 -1 1])
    xlabel('x');
    ylabel('y');
    title(sprintf('Dataset %i',i));
    display('Input spurious samples');
    [x,y] = getpts(gcf);
    samples{i} = [nonspurious1; nonspurious2; [x, y]];
    labels{i} = [zeros(length(nonspurious1), 1); ones(length(nonspurious2), 1); zeros(size(x))];
    spurious{i} = [zeros(length(nonspurious1)+length(nonspurious2), 1); ones(size(x))];
    clf, hold on
end
    