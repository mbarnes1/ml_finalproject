function [ fh, axisAll ] = scatall( samples, labels, varargin )
%SCATALL Initial scatter plot of all the data
%   Inputs:
%       samples - (ndatasets x 1) cell array of (nsamples x 2) array of
%                 samples
%       labels - (ndatasets x 1) cell array of (nsamples x 1) array of
%                 class labels
%       varargin - (ndatasets x 1) cell array of (nsamples x 1) array of
%                 spurious labels
%
%   Outputs:
%       fh - figure handle of scatter plot
%       axisAll - (2 x #dim) array of axis min (first row) and max (second
%       row) along each dimension
%
%   Author:
%       Matt Barnes

if isempty(varargin)
    spurious = cell(size(labels));
    for i = 1:length(spurious)
        spurious{i} = zeros(size(labels{i}));
    end
else
    spurious = varargin{1};
end
ndatasets = length(samples);
ndim = size(samples{1}, 2);
axisAll = NaN(ndatasets, 4);
fh = figure;
sp_shape = [ceil(ndatasets/ceil(sqrt(ndatasets))), ceil(sqrt(ndatasets))];  % shape of the subplot for good visualization
for i = 1:length(samples)
    subplot(sp_shape(1), sp_shape(2), i);
    hold on;
    scatter(samples{i}(labels{i}==1 & spurious{i}==0, 1), ...
        samples{i}(labels{i}==1 & spurious{i}==0, 2), 'g+'); % pos class nonspurious
    scatter(samples{i}(labels{i}==1 & spurious{i}>0, 1), ...
        samples{i}(labels{i}==1 & spurious{i}>0, 2), 'g*'); % pos class spurious
    scatter(samples{i}(labels{i}==0 & spurious{i}==0, 1), ...
        samples{i}(labels{i}==0 & spurious{i}==0, 2), 'r+'); % neg class nonspurious
    scatter(samples{i}(labels{i}==0 & spurious{i}>0, 1), ...
        samples{i}(labels{i}==0 & spurious{i}>0, 2), 'r*'); % neg class spurious
    scatter(samples{i}(labels{i}==0 & spurious{i}>0, 1), ...
        samples{i}(labels{i}==0 & spurious{i}>0, 2), 75,'bo'); % neg class spurious
    
    xlabel('x1'), ylabel('x2')
    title(['Training Set ', num2str(i)])
    %legend('Negative, non-spurious','Negative, spurious','Positive, non-spurious','Positive, spurious')
    axisAll(i, :) = [min(samples{i}(:,1)), max(samples{i}(:,2)), min(samples{i}(:,1)), max(samples{i}(:,2))];
end

axisAll = [min(axisAll(:, 1)), max(axisAll(:, 2)), min(axisAll(:, 3)), max(axisAll(:, 4))];
for i = 1:ndatasets
    subplot(ceil(ndatasets/ceil(sqrt(ndatasets))), ceil(sqrt(ndatasets)), i);
    axis(axisAll);
end

axisAll = reshape(axisAll, 2, ndim);

end

