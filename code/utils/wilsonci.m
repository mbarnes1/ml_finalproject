function [ ci ] = wilsonci( p, n )
%WILSONCI Wilson Confidence Intervals
%   Inputs:
%       p - column vector of TPR or FPR
%       n - (scalar) number of corresponding positives or negatives, respectively

z = 1.96;  % for 95% confidence interval
upper = 1/(1+1/n*z^2) * (p + 1/(2*n)*z^2 + z*sqrt(1/n*p.*(1-p)+1/(4*n^2)*z^2));
lower = 1/(1+1/n*z^2) * (p + 1/(2*n)*z^2 - z*sqrt(1/n*p.*(1-p)+1/(4*n^2)*z^2));
ci = [lower, upper];

end

