function [models_pos, models_neg] = generateModels(nspurious, ndatasets)
%GENERATEMODEL Creates an underlying model of type spurious or
%nonspurious. Model follows a normal distribution
%   Inputs:
%       nspurious - number of spurious components
%       ndatasets - number of datasets
%
%   Outputs:
%       models_pos - structure of positive class training GMMs. First components are nonspurious
%       models_neg - structure of negative class training GMMs. First components are nonspurious
%
%   Author:
%       Matt Barnes

% TO DO: Randomly generate full spectrum of valid Sigma (any 2x2 matrix
% with non-negative determinant?)

if nspurious<0
    error('generateModels:invldInput','Input must be an integer >= 0');
end

ndim = 2;  % to add later

% initialize outputs
models_pos = cell(ndatasets, 1);
models_neg = cell(ndatasets, 1);

for i = 1:ndatasets
    mu_pos = NaN(nspurious+1, 2);
    mu_neg = NaN(nspurious+1, 2);
    Sigma = NaN(2,2,nspurious+1);
    weights = NaN(1,nspurious+1);
    
    % nonspurious component
    mu_pos(1,:) = [0.25; 0.25];
    mu_neg(1,:) = [-0.25; -0.25];
    Sigma(:,:,1) = 0.05*eye(2,2);
    weights(1) = 1;
    
    % spurious components
    for j = 2:nspurious+1
        mu = 6*(rand(1, 2)-0.5);
        angle = randn(1, ndim);
        angle = 1/3*angle./sqrt(sum(angle.^2, 2));
        mu_pos(j,:) = mu+angle;
        mu_neg(j,:) = mu-angle;
        Sigma(:,:,j) = 0.1*rand(1)*eye(2);
        weights(j) = 0.1;
    end
    
    models_pos{i} = gmdistribution(mu_pos,Sigma,weights);
    models_neg{i} = gmdistribution(mu_neg,Sigma,weights);
end

end

