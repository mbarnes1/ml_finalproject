%i**********************************************************************************
%
%	LOAD_DATA
%
%		Function used to load an incoming data set and prepare it for
%		spurious sample removal
%
%		INPUT: filepath -> path to the data
%
%		OUTPUT: samples -> cell array


function [samples, labels, sets, samples_testing, labels_testing, sets_testing] = getnuclear(filepath,numSamples)

load(filepath);

% Pull out sets labels and class labels
sets = data(:,1);
samples = data(:,[21 42 43 69]); % Only use six features
labels = data(:,end);


% Split into testing and training
trainInd = 2:max(sets);
samples_training = samples(ismember(sets,trainInd), :);
labels_training = labels(ismember(sets,trainInd));
sets_training = sets(ismember(sets,trainInd));

testInd = 1;
samples_testing = samples(ismember(sets,testInd), :);
labels_testing = labels(ismember(sets,testInd));
sets_testing = sets(ismember(sets,testInd));


% Nick's old code
% idx=unique(data(:,1));
% assign_idx=randperm(length(idx));
% assign_idx=idx(assign_idx);
% 
% 
% numSets=size(idx,1);
% numSamples=numSamples*numSets;
% 
% ds_size=size(data,1);
% %interval=floor(ds_size/numSamples);
% 
% %Subsampling for speed
% y=randsample(ds_size,numSamples);
% data=data(y,:);
% 
% split_train=cell(1);
% split_test=cell(1);
% split_train_l=cell(1);
% split_test_l=cell(1);
% train_foldID=[];
% test_foldID=[];
% 
% % Only using first fold as testing
% for ceil(numSets/2)
% 	single_set=data(data(:,1)==assign_idx(i),2:end-1);
% 	single_labels=data(data(:,1)==assign_idx(i),end);
% 	
% 	%single_set=single_set(1:interval:end,2:end);
% 	%single_labels=single_labels(1:interval:end);
% 	
% 	split_train{i,1}=single_set;
% 	split_train_l{i,1}=single_labels;
% 	train_foldID=[train_foldID; assign_idx(i)];
% end
% 
% for ceil(numSets/2)+1:numSets
% 	single_set=data(data(:,1)==assign_idx(i),2:end-1);
% 	single_labels=data(data(:,1)==assign_idx(i),end);
% 
% 	%single_set=single_set(1:interval:end,2:end);
% 	%single_labels=single_labels(1:interval:end);
% 
% 	split_test{i-ceil(numSets/2),1}=single_set;
% 	split_test_l{i-ceil(numSets/2),1}=single_labels;
% 	test_foldID=[test_foldID; assign_idx(i)];
% end
% 
% samples=split_train;
% labels=split_train_l;
% samples_testing=split_test;
% labels_testing=split_test_l;
