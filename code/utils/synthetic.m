function [samples, labels, spurious] = ...
    synthetic(nsamples, ndatasets, varargin )
%SYNTHETIC Generates synthetic data
%   Inputs
%       nsamples - number of samples
%
%       OPTIONAL & PARAMETERS
%       'spurious',nspurious - number of spurious components: default=1
%
%   Outputs
%       samples - cell array of samples
%       labels - cell array of spurious labels
%       spurious - cell array of spurious labels


%% Parse Inputs
p = inputParser;

defaultSpurious=1;

addRequired(p,'nsamples',@isnumeric);
addParamValue(p,'spurious',defaultSpurious,@isnumeric);

parse(p,nsamples,varargin{:});

nspurious = p.Results.spurious;
nsamples  = p.Results.nsamples;

[models_pos, models_neg] = generateModels(nspurious, ndatasets);

%% Sample points from the model
% Nonspurious: label=0  Spurious: label>=1
samples = cell(ndatasets, 1);
labels = cell(ndatasets, 1);
spurious = cell(ndatasets, 1);
for i = 1:ndatasets
    [samples_neg, labels_neg] = random(models_neg{i}, int32(nsamples/2));
    [samples_pos, labels_pos] = random(models_pos{i}, int32(nsamples/2));
    labels{i} = [zeros(size(labels_neg)); ones(size(labels_pos))];  % 0=neg, 1=pos
    samples{i} = [samples_neg; samples_pos];
    spurious{i} = [labels_neg; labels_pos]-1;  % 0=nonspurious, >=1 spurious
end
clear samples_neg samples_pos labels_neg labels_pos

end

