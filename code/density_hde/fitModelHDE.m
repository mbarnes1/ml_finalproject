function model = fitModelHDE( samples, varargin )
%FITMODEL Fits a bivariate histogram density estimate to the data
%   Inputs:
%       samples - (nsamples x 2) sample points
%       C (optional) - bin centers to use
%
%   Outputs:
%       model - bin center probabilities
%       C - bin centers
%
%   Author:
%       Matt Barnes

nVarargs = length(varargin);
if nVarargs>0
    C = varargin{1};  % use previous centers
else
    [~, C] = hist3(samples,[10 10]);  % 10x10 bivariate histogram
end

N = hist3(samples, C);
model = N/sum(sum(N));

end

