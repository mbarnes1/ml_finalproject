function [ model ] = fitModelGMM( data )
%FITMODEL Fits a bivariate normal distribution to the data
%   Inputs:
%       data - nx2 sample points
%
%   Outputs:
%       model - Gaussian Mixture Model
%
%   Author:
%       Matt Barnes

model = gmdistribution.fit(data,1);  % fit one multivariate normal to the data

end

