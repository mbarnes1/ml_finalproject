function [ D ] = klHDEmulti( samples, labels, sets, bins)
%KLHDEMULTI Computes the average pairwise divergence between all sample
%sets
%   Inputs:
%       samples - (nsamples x 2) array of samples
%       labels - (nsamples x 1) class labels
%       sets - (nsamples x 1) dataset label
%
%   Output:
%       D - average pairwise KL divergence between datasets

nsets = max(sets);
nlabels = max(labels)+1;
models = cell(nsets, nlabels);
for i = 1:nsets
    for j = 1:nlabels
        models{i,j} = fitModelHDE(samples(labels==(j-1) & sets==i, :), bins);
    end
end

comb = nchoosek(1:nsets, 2);
comb = [comb; fliplr(comb)];  % divergences in both directions

D = zeros(length(comb),1);

for i = 1:length(D)
    for j = 1:nlabels
        D(i) = D(i) + kullback_leibler_HDE(models{comb(i,1), j}, models{comb(i,2), j});
    end
end

D = mean(D);


end

