function [] = plot_gmm(model, varargin)
%PLOT_ALL Plots the pdf of model as heatmap
%   Inputs
%       model - a GMM
%       p (varargin) - probability of model, default = 1
%
%   Author:
%       Matt Barnes

nVarargs = length(varargin);
if nVarargs>0
    p = varargin{1};
else
    p = 1;
end

x1 = -3:.05:3; x2 = -3:.05:3;
[X1,X2] = meshgrid(x1,x2);
X = [X1(:), X2(:)];

F = pdf(model, X);
F = p*reshape(F, length(x2), length(x1));

surf(x1,x2,F,'LineStyle',':');
axis('auto')
axis([-3 3 -3 3 0 max(F(:))])
xlabel('x1'); ylabel('x2'); zlabel('Probability Density');



end

