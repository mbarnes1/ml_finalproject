function [ KL ] = kullback_leibler_GMM(model0, model1, varargin)
%KULLBACK_LEIBLER Computes the kullback-leibler divergence between two GMMs
% using Monte Carlo sampling (Hershey 2007)
%
%   Inputs:
%       model0 & model1 - GMMs
%       p (varargin) - the probabilities of model0 and model1 (default 1)
%
%   Output:
%       KL - divergence between model0 & model1

nVarargs = length(varargin);
if nVarargs>0
    p0 = varargin{1};
    p1 = varargin{2};
else
    p0 = 1;
    p1 = 1;
end

n = 1000000;  % number of Monte Carlo samples to draw from model0
              % this computes D(f||g). What about D(g||f)?
              
x = random(model0, n);  % draw n samples from the GMM
fx = p1*pdf(model0, x)+realmin;  % f(x) w/ adding realmin to smooth zeros
gx = p0*pdf(model1, x)+realmin;  % g(x) w/ adding realmin to smooth zeros

KL = 1/n*sum(log(fx./gx));  % the KL divergence


end
