function [ KL ] = kullback_leibler_HDE(model0, model1)
%KULLBACK_LEIBLER Computes the kullback-leibler divergence between two
%Histogram Density Estimates
%   Inputs:
%       model0 - a HDE with bin center C
%       model1 - a cell array of HDEs with the same bin centers C
%
%   Output:
%       KL - (1 x length(model1)) array of divergences between model0 & model1's


KL_temp = log(model0./(model1+0.0001)).*model0;  % KL for discrete PDFs
KL_temp(isnan(KL_temp))=0;  % treat nans as zero
KL_temp = sum(sum(KL_temp));  % total KL divergence
KL = KL_temp;


end
