function [ D ] = itemulti( samples, labels, sets, model, fh )
%ITEMULTI Performs multiple calls of ITE divergence estimates
%   Inputs:
%       samples - array of (nsamples x 2) samples
%       labels - vector of (nsamples x 1) class labels
%       sets - vector of (nsamples x 1) set labels
%       model - divergence model initialized by ITE
%       fh - function handle for ITE estimation function
%
%   Outputs:
%       D - global divergence, the average of all dataset pairwise divergences
%
%   Author:
%       Matt Barnes

comb = nchoosek(1:max(sets), 2);
comb = [comb; fliplr(comb)];  % divergences in both directions

D = NaN(length(comb),1);

for i = 1:length(D)
    try
        D(i) = fh(samples(sets==comb(i,1) & labels==0, :)', samples(sets==comb(i,2) & labels==0, :)', model) + ...
            fh(samples(sets==comb(i,1) & labels==1, :)', samples(sets==comb(i,2) & labels==1, :)', model);
    catch exception
        D(i) = Inf;
    end
end

D = mean(D);

end

