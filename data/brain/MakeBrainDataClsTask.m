% This script makes a classification task from the available voxel data
% and semantic features for the shelter/manipulable categories
clear;
category = 'Manipulable';
load(sprintf('BrainData_%s.mat',category));
intel_features = eval(sprintf('IntelFeatures_%s',category));
% mark any word which is rated at least a 4 in one feature as
% belonging to the category.
Y = max(double(intel_features>=4),[],2);
% some manual corrections
if (strcmp(category,'Shelter'))
    Y(24) = 0; % 'coat' is not shelter
    Y(29) = 0; % 'dog' is not shelter
    Y(28) = 0; % 'desk' is not shelter
    %Y(30) = 0; % 'door' is not shelter
    Y(31) = 0; % 'dress' is not shelter
    Y(36) = 0; % 'glass' is not shelter
    Y(43) = 0; % 'knife' is not shelter
    Y(46) = 0; % 'pants' is not shelter
    Y(51) = 0; % 'shirt' is not shelter
    Y(52) = 0; % 'skirt' is not shelter
    %Y(60) = 0; % 'window' is not shelter
end
if (strcmp(category,'Manipulable'))
    Y(8) = 0; % 'bed' is not manipulable
    Y(19) = 0;  % 'chair' is not manipulable
    Y(28) = 0;  % 'desk' is not manipulable
    Y(33) = 0;  % 'eye' is not manipulable
    Y(35) = 0;  % 'foot' is not manipulable
    Y(36) = 0;  % 'glass' is not manipulable
    Y(39) = 0;  % 'horse' is not manipulable
    Y(44) = 0;  % 'leg' is not manipulable
    Y(54) = 0;  % 'table' is not manipulable
    Y(58) = 0;  % 'truck' is not manipulable
end
brain_data = eval(sprintf('Voxels%s',category));
for i=1:length(brain_data)
    brain_data{i} = horzcat(brain_data{i},Y);
end
save(sprintf('Classification_%s.mat',category),'brain_data');