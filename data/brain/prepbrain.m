load('Classification_Manipulable.mat')
M=brain_data;
Mmat=[];

load('Classification_Shelter.mat')
S=brain_data;
Smat=[];

for i=1:size(M,2)
	Mmat=[Mmat;[i*ones(size(M{i},1),1),M{i}]];
end

for i=1:size(S,2)
	Smat=[Smat;[i*ones(size(S{i},1),1),S{i}]];
end

data=Mmat;

save('manipulable.mat','data')

data=Smat;

save('shelter.mat','data')
