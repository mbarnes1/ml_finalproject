CPD README
Matt Barnes

The Basics - How to run this code on your dataset
1) Get all the required libraries. They are: libsvm-3.18, its, odToolbox, kdtree1.2, and a customized git wrapper. Don’t worry, I have them all aggregated in a separate folder, however you will need to run a few install commands.
—> Clone the git repo ‘matlab’ from bitbucket into ~/Documents/. Email me for access
—> Install libsvm-3.18 by running its make.m file in MATLAB.
—> Install the its toolbox by running ITE_install.m
—> Install kdtree by creating mex files for all 6 .cpp files. Just copy and paste “mex kdtree_ball_query.cpp; mex kdtree_build.cpp; mex kdtree_delete.cpp; mex kdtree_k_nearest_neighbors.cpp; mex kdtree_nearest_neighbor.cpp; mex kdtree_range_query.cpp” into matlab, without quotations. You’ll need to be in the folder with these files.

2) Format your data into samples, labels, spurious, samples_testing, labels_testing. Each of these are a cell array, where each cell is a separate ‘fold.’ The fastest way for you to understand how to do this to look at an example in /data/synthetic/hand3.mat. Since you don’t know which points are spurious, you can just make this variable all zeros.

3) Input parameters in RUNME.m
—> Specify the path to your .mat datafile with parameters.datafile
—> numCores - number of cores to parallelize with
—> parameters.nremove - number of samples to remove (err on the too large side, as you will get results for every value less than this)
—> parameters.approaches - string of the approach you want to use, either an ITE divergence estimate or SVM. Check out all the options in the code.
—> parameters.nboot - how many bootstraps to compute (I recommend 10)
All the other parameters can usually be left alone.



Other Really Freakin’ Cool Features - . 
parameters.git automatically gets the current version of the code, in the form of the git hash. Sounds confusing, but all this means is if you view ‘Commits’ at https://bitbucket.org/mbarnes1/ml_finalproject/commits/all, that’s the exact copy of code used to generate that results file! So you’ll always know how all the results were generated.



How to Add a Competitor - 
Create your competitor function of the form
ind = Function(samples, labels, spurious, nremove)
and save it in the benchmarks folder. ind is a binary vector of the samples removed (0) and not removed (1). See some of the examples there for guidance.

The only other file you need to change is bootstrpcustom. You’ll need to create the appropriate variables for your method. Look at examples Baseline, Common, and Localreconstruction for how to do this (should be pretty straight forward).